'use strict';
var webpack = require('webpack'),
    path = require('path'),
    CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: __dirname + '/app',
  entry: {
    app: [
      'webpack/hot/dev-server',
      './bootstrap.js',
    ]
  },
  output: {
    path: __dirname + '/build',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.html$/,
        loader: 'file',
      },
      {
        test: /bootstrap\/js\//,
        loader: 'imports?jQuery=jquery'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.js$/,
        loaders: [
          'ng-annotate',
          'babel?presets[]=es2015',
          'eslint'
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.(otf|eot|svg|ttf|woff)/,
        loader: 'url-loader?limit=8192'
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([
      {
        from: 'index.html'
      },
    ]),
  ]
};
