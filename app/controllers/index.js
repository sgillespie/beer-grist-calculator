import angular from 'angular';

export default angular
  .module('controller', [])
  .controller('targets', ['$scope', 'grainStore', 'grainWeights',
      ($scope, store, grainWeights) => {
        $scope.grainWeights = grainWeights;
        $scope.targets = store.targets;
        $scope.isValid = () => {
          const { percentage } = grainWeights.totals;
          return percentage === 100 || store.grains.length === 0;
        };
        $scope.onChangeTargets = () => {
          grainWeights.update(store);
        };
      }])
  .controller('grains', ['$scope', 'grainStore', 'grainWeights',
    ($scope, store, grainWeights) => {
      grainWeights.update(store);
      $scope.grains = grainWeights;
      $scope.isValid = () => {
        const { percentage } = grainWeights.totals;
        return percentage === 100 || store.grains.length === 0;
      };
      $scope.deleteClicked = id => {
        store.deleteGrain(id);
        grainWeights.update(store);
      };
    }])
  .controller('grainInput', ['$scope', 'grainStore', 'grainWeights',
    ($scope, store, weights) => {
      $scope.grainInput = {};
      $scope.addClicked = (form) => {
        if (form.$invalid) {
          return;
        }

        const { type, percentage, ppg } = $scope.grainInput;

        store.addGrain(type, percentage, ppg);
        weights.update(store);

        $scope.grainInput = {};
        form.$setPristine();
      };
    }]);
