import $ from 'jquery';
import angular from 'angular';

export default angular
  .module('directives', [])
  .directive('bgcShowErrors', ['$animate', ($animate) => {
    return {
      restrict: 'A',
      require: '^form',
      link (scope, element, attrs, formController) {
        const jqElement = $(element),
              input = jqElement.find('input[name]'),
              name = input.attr('name'),
              help = jqElement.find('.help-block'),
              isValid = () => {
                const elementCtrl = formController[name];
                return elementCtrl.$valid || elementCtrl.$pristine;
              };

        scope.$watch(isValid, value => {
          const action = val => val ? 'addClass' : 'removeClass';

          $animate[action(!value)](element, 'has-error');
          $animate[action(value)](help, 'ng-hide', {
            tempClasses: 'ng-hide-animate',
          });
        });
      },
    };
  }]);
