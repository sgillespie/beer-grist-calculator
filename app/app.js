import angular from 'angular';
import controller from './controllers';
import directives from './directives';
import filters from './directives/filters';
import services from './services';

export default angular.module('app', [
  controller.name,
  directives.name,
  filters.name,
  services.name,
]);
