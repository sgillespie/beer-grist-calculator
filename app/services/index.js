import angular from 'angular';
import isEmpty from 'lodash/lang/isEmpty';
import max from 'lodash/math/max';
import merge from 'lodash/object/merge';
import remove from 'lodash/array/remove';

export default angular
  .module('services', [])
  .factory('grainStore', () => {
    return {
      grains: [
        /** Example:
         *
         * {
         *   type: 'Two Row (US)',
         *   percentage: 90,
         *   yield: 1.036,
         * },
         **/
      ],

      targets: {
        gallons: 5,
        gravity: 1.045,
        efficiency: 70,
      },

      calculateTotalWeight () {
        const { grains, targets } = this,
              { gravity, efficiency, gallons } = targets,
              points = sg => (sg - 1) * 1000, 	// Gravity points
              targetPpg = grains.reduce((accum, grain) => {
                return accum + (grain.percentage / 100 * grain.ppg);
              }, 0);

        return (gallons * points(gravity)) /
          (efficiency / 100 * points(targetPpg));
      },

      calculateGrainWeights () {
        const totalWeight = this.calculateTotalWeight(),
              isValid = this.calculateTotalPercentage() === 100;

        return this.grains.map(grain => {
          const weight = grain.weight(totalWeight);

          return merge({}, grain, {
            weightLbs: isValid ? weight.lbs : 0,
            weightOz: isValid ? weight.oz : 0,
          });
        }, true);
      },

      calculateTotalPercentage () {
        return this
          .grains
          .reduce((accum, val) => accum + val.percentage, 0);
      },

      calculateTotals () {
        const { gallons, gravity } = this.targets,
              totalWeight = this.calculateTotalWeight(),

              weightLbs = Math.floor(totalWeight),
              weightOz = (totalWeight - weightLbs) * 16,
              percentage = this.calculateTotalPercentage(),
              gravityPts = (gravity - 1) * 1000 * gallons;

        return {
          percentage,
          weightLbs: percentage === 100 ? weightLbs : 0,
          weightOz: percentage === 100 ? weightOz : 0,
          gravityPts,
        };
      },

      addGrain (type, percentage, ppg) {
        const { grains } = this,
              id = grains_ => {
                if (isEmpty(grains_)) return 0;

                return 1 + max(grains_, grain => grain.id).id;
              };

        grains.push({
          id: id(grains),
          type,
          percentage,
          ppg,

          weight (total) {
            const weight = this.percentage / 100 * total,
                  lbs = Math.floor(weight),
                  oz = (weight - lbs) * 16;

            return {
              lbs,
              oz,
            };
          },
        });
      },

      deleteGrain (id) {
        return remove(this.grains, grain => grain.id === id);
      },
    };
  })
  .value('grainWeights', {
    grains: [],
    totals: {},
    update (store) {
      this.grains = store.calculateGrainWeights();
      this.totals = store.calculateTotals();
    },
  });
