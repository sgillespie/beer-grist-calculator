import angular from 'angular';
import vendor from './vendor';
import app from './app';

vendor();

require('bootstrap/dist/css/bootstrap.min.css');
require('../assets/app.css');

angular.element(document).ready(function () {
  angular.bootstrap(document, [app.name]);
});
