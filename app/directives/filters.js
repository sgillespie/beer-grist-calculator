import angular from 'angular';

export default angular
  .module('filters', [])
  .filter('percent', function () {
    return function (input) {
      const percentage = input * 100;
      return `${percentage}%`;
    };
  })
  .filter('floor', function () {
    return Math.floor;
  });
